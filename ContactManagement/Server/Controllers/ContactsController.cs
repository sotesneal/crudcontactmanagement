﻿using ContactManagement.Server.Interfaces;
using ContactManagement.Shared.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ContactManagement.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly IContactService _contactService;
        public ContactsController(IContactService contactService)
        {
            _contactService = contactService;
        }
        //generate all the methods for the contact controller
        //add try catch blocks to all the methods
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Contact>>> GetContacts(int page, int pageSize)
        {
            try
            {
                var contacts = await _contactService.ReadContacts(page, pageSize);
                return Ok(contacts);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<Contact>> GetContact(int id)
        {
            try
            {
                var contact = await _contactService.ReadContact(id);
                return Ok(contact);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpPost]
        public async Task<ActionResult<Contact>> CreateContact([FromBody]Contact contact)
        {
            try
            {
                var newContact = await _contactService.CreateContact(contact);
                return Ok(newContact);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Contact>> UpdateContact(int id,[FromBody]Contact contact)
        {
            try
            {
                var existingContact = await _contactService.ReadContact(id);
                if (existingContact == null)
                {
                    return NotFound($"Contact with Id = {id} not found");
                }

              
                existingContact.FirstName = contact.FirstName;
                existingContact.LastName = contact.LastName;
                existingContact.Email = contact.Email;
                existingContact.Status = contact.Status;
                existingContact.Address = contact.Address;
                existingContact.UpdatedAt = DateTime.Now;
                var updatedContact = await _contactService.UpdateContact(existingContact);
                return Ok(updatedContact);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteContact(int id)
        {
            try
            {
                await _contactService.DeleteContact(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("search")]
        public async Task<ActionResult<IEnumerable<Contact>>> Search(string name)
        {
            var contacts = await _contactService.SearchContact(name);
            if (contacts.Any())
            {
                return Ok(contacts);
            }
            else
            {
                return NotFound("No contacts matching the search term were found.");
            }
        }
        [HttpGet("total")]
        public async Task<ActionResult<int>> GetTotalContacts()
        {
            try
            {
                var totalContacts = await _contactService.GetTotalContacts();
                return Ok(totalContacts);
            }
            catch (Exception ex)
            {
                // Handle the exception and return an appropriate error response
                Console.WriteLine($"An error occurred: {ex.Message}");
                return StatusCode(500, "An error occurred while retrieving the total contact count.");
            }
        }
    }
}
