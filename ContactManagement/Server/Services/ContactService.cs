﻿using ContactManagement.Server.Data;
using ContactManagement.Server.Interfaces;
using ContactManagement.Shared.Models;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace ContactManagement.Server.Services
{
    public class ContactService : IContactService
    {
        private readonly ContactDbContext _context;
        public ContactService(ContactDbContext contactDbContext)
        {
            _context = contactDbContext;
        }
        public async Task<Contact> CreateContact(Contact contact)
        {
            TextInfo textInfo = CultureInfo.CurrentCulture.TextInfo;
            contact.FirstName = textInfo.ToTitleCase(contact.FirstName);
            contact.LastName = textInfo.ToTitleCase(contact.LastName);
            contact.Address = textInfo.ToTitleCase(contact.Address);
            // Check if a contact with the same first name and last name already exists
            var existingContact = await _context.Contacts
                .FirstOrDefaultAsync(c =>
                    c.FirstName.ToUpper() == contact.FirstName.ToUpper() &&
                    c.LastName.ToUpper() == contact.LastName.ToUpper());
            if (existingContact != null)
            {
                throw new Exception("Contact with the same first name and last name already exists");
            }
            _context.Contacts.Add(contact);
            await _context.SaveChangesAsync();
            return contact;
        }
        //generate all the methods for the contact service
        public async Task DeleteContact(int id)
        {
            var contactToDelete = await _context.Contacts.FindAsync(id);
            if (contactToDelete != null)
            {
                _context.Contacts.Remove(contactToDelete);
                await _context.SaveChangesAsync();
            }
        }
        public async Task<IEnumerable<Contact>> ReadContacts(int page, int pageSize)
        {
            var contacts = await _context.Contacts
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync();
            return contacts;
        }
        public async Task<Contact> ReadContact(int id)
        {
           var contact = await _context.Contacts.FindAsync(id);
            if (contact == null) 
            {
                throw new Exception("Contact not found");
            }
            return contact;
        }
        public async Task<Contact> UpdateContact(Contact contact)
        {
            TextInfo textInfo = CultureInfo.CurrentCulture.TextInfo;
            contact.FirstName = textInfo.ToTitleCase(contact.FirstName);
            contact.LastName = textInfo.ToTitleCase(contact.LastName);
            contact.Address = textInfo.ToTitleCase(contact.Address);
            
            _context.Entry(contact).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return contact;
        }
        public async Task<IEnumerable<Contact>> SearchContact(string term)
        {
            var contacts = await _context.Contacts
                .Where(c => c.FirstName.Contains(term) || c.LastName.Contains(term) || c.Status.Contains(term) || c.Address.Contains(term))
                .ToListAsync();

            return contacts ?? Enumerable.Empty<Contact>();
        }
        public async Task<int> GetTotalContacts()
        {
            return await _context.Contacts.CountAsync();
        }
    }

}
