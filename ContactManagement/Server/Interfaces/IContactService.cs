﻿using ContactManagement.Shared.Models;

namespace ContactManagement.Server.Interfaces
{
    public interface IContactService
    {
        //generate all the methods for the contact service
        public Task<IEnumerable<Contact>> ReadContacts(int page,int pageSize);
        public Task<Contact> ReadContact(int id);
        public Task<Contact> CreateContact(Contact contact);
        public Task<Contact> UpdateContact(Contact contact);
        public Task DeleteContact(int id);
        public Task<IEnumerable<Contact>> SearchContact(string term);
        public Task<int> GetTotalContacts();


    }
}
