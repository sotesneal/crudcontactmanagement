﻿using ContactManagement.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace ContactManagement.Server.Data
{
    public class ContactDbContext : DbContext
    {
        public ContactDbContext(DbContextOptions<ContactDbContext> options) : base(options)
        {
        }
        //generate an onmodelcreating method to set the lastname and firstname combinations a unique
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>()
                .HasIndex(u => new { u.FirstName, u.LastName })
                .IsUnique();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Contact> Contacts { get; set; } = null!; 
    }
}
